// Get form object and fields
const signUpForm = document.querySelector("#sign-upForm");
const nameField = document.querySelector("#name");
const emailField = document.querySelector("#email");
const messageField = document.querySelector("#message");
const submitButton = document.querySelector("#submitButton");

let oldStyle = emailField.parentElement.querySelector(
    ".invalid-feedback-empty"
).style;

function isEmpty(fieldString) {
    return fieldString === null || fieldString.match(/^ *$/) !== null;
}

function emailIsValid() {
    // Assign Default Variables
    let email = emailField.value;

    // Check if email is empty
    let emailIsEmpty = isEmpty(email);

    // Validation is done using regex
    let validEmail =
        email.match(
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        ) !== null;

    if (emailIsEmpty === true) {
        emailField.parentElement.querySelector(
            ".invalid-feedback-empty"
        ).style.display = "block";
    } else {
        emailField.parentElement.querySelector(
            ".invalid-feedback-empty"
        ).style.display = "none";
        if (validEmail) {
            emailField.parentElement.querySelector(
                ".invalid-feedback"
            ).style.display = "none";
        } else {
            emailField.parentElement.querySelector(
                ".invalid-feedback"
            ).style.display = "block";
        }
    }

    console.log(`Valid Email: ${validEmail}`);
    return validEmail;
}

function isNameEmpty() {
    let nameEmpty = isEmpty(nameField.value);
    if (nameEmpty) {
        nameField.parentElement.querySelector(
            ".invalid-feedback"
        ).style.display = "block";
    } else {
        nameField.parentElement.querySelector(
            ".invalid-feedback"
        ).style.display = "none";
    }

    return nameEmpty;
}

// Check for form validity
function formIsValid() {
    console.log(`Name Empty: ${isNameEmpty()} email valid: ${emailIsValid()}`);
    return !isNameEmpty() && emailIsValid();
}

// Send Form Data
console.log("Sending Data...");

// Intercept Submit event and submit to google sheets
window.addEventListener("load", function () {

    const form = signUpForm;

    form.addEventListener("submit", function (e) {
        e.preventDefault();

        // Check form validation
        if (formIsValid()) {
            console.log("Form Submitted!");
            document.querySelector("#submitErrorMessage").style.display = "none";
        } else {
            console.log("Submission Failed!");
            document.querySelector("#submitErrorMessage").style.display = "block";
        }

        // Show submitting form...
        const successMessage = document.querySelector("#submitSuccessMessage");
        const successText = document.querySelector("#successText");
        successText.classList.add("text-muted");
        successText.textContent = "Submitting Form...";
        successMessage.classList.remove("d-none");

        // Grey out submit temporarily
        submitButton.classList.add("disabled");

        // Send Form Data
        const data = new FormData(form);
        const action = e.target.action;
        fetch(action, {
            method: "POST",
            body: data,
        })
            .then(function () {
                console.log("FETCH COMPLETE!");

                // Show form submitted confirmation
                successText.classList.remove("text-muted");
                successText.textContent = "Form submitted";
            })
            .catch(function (err) {
                console.error(err);
            });
    });
});
